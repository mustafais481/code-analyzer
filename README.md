# Analyseur de Code JavaScript

Ce projet, réalisé en groupe, développe un analyseur de code en Python pour évaluer la qualité du code JavaScript. Il intègre l'analyse syntaxique, la tokenization, et l'évaluation basée sur divers critères de qualité du code, organisés en modules spécifiques.

## Membres du Groupe

- ISMAIL Mustafa
- KOLINGBA NZANGA Aaron
- FREJABUE Anais
- LATEB Melissa

## Structure du Projet

- **Parser** : Analyse le code JavaScript pour générer l'Arbre Syntaxique Abstrait (AST).
- **Tokenizer** : Convertit le code en tokens pour faciliter l'analyse syntaxique.
- **Scoring** : Évalue la qualité du code à l'aide de différents critères et attribue un score.

## Comment Utiliser

1. Clonez le dépôt : `git clone https://gitlab.com/mustafais481/code-analyzer.git`
2. Exécutez le programme principal : `python3 main.py`

## Fonctionnalités Principales

- **Tokenization** : Sépare le code en éléments significatifs comme les identifiants, les littéraux, etc.
- **Parsing** : Construit l'AST pour refléter la structure logique du code.
- **Scoring** : Note le code en fonction de l'usage des variables, de l'indentation, et d'autres critères de qualité.
