def allDeclaredIsUsed(code):
    max_score = 10
    declared_variables = set()
    used_variables = set()
    for node in code:
        if node["type"] == "variableDeclaration":
            declared_variables.add(node["variableName"])
        elif node["type"] == "word" and node["value"] not in ["console", "log"]:
            if node["value"]:
                used_variables.add(node["value"]) 
    
    for v in declared_variables:
        if v not in used_variables and max_score > 0:
            max_score = max_score - 1
            
    return max_score


def allUsedIsDeclared(code):
    max_score = 10
    declared_variables = set()
    used_variables = set()
    for node in code:
        if node["type"] == "variableDeclaration":
            declared_variables.add(node["variableName"])
        elif node["type"] == "word" and node["value"] not in ["console", "log"]:
            if node["value"]:
                used_variables.add(node["value"]) 
    
    for v in used_variables:
        if v not in declared_variables and max_score > 0:
            max_score = max_score - 1
            
    return max_score

def allExpressionFinished(ast):
    score = 10
    string_errors = 0
    variable_value_errors = 0

    for i, node in enumerate(ast):
        # Vérifier si les valeurs des chaînes finissent par un point
        if node['type'] == 'variableAffectation':
            variable_value = node['variableValue']
            # Pour les chaînes assignées à des variables
            if variable_value['type'] == 'string' and not variable_value['value'].endswith('.'):
                variable_value_errors += 1
        # Pour les chaînes directement utilisées (ex: dans un console.log)
        elif node['type'] == 'quotationMark':
            # Trouver la fin de la chaîne
            end_index = i + 1
            while end_index < len(ast) and ast[end_index]['type'] != 'quotationMark':
                end_index += 1
            # Vérifier si la dernière partie de la chaîne (avant le guillemet fermant) ne finit pas par un point
            if end_index < len(ast) and ast[end_index - 1]['type'] == 'word' and not ast[end_index - 1]['value'].endswith('.'):
                string_errors += 1

    # Appliquer une pénalité pour les erreurs trouvées
    if string_errors > 0:
        score -= min(2, string_errors)  # Une pénalité jusqu'à 2 points pour les erreurs de chaîne

    if variable_value_errors > 0:
        score -= min(2, variable_value_errors)  # Une pénalité jusqu'à 2 points pour les erreurs de valeurs des variables

    return max(0, score)

def indentation(ast):
    max_score = 10
    for node in ast:
        if node['type'] == 'indentation':
            max_score -= 1
    return max_score

def numberLine(ast):
    line_count = 1
    max_lines = 200
   
    for node in ast:
        if node['type'] == 'newLine':
            line_count += 1
   
    if line_count > max_lines:
        score_value = 0
    else:
        score_value = 10


    return score_value

def calculate_score(ast):
    all_declared_used_score = allDeclaredIsUsed(ast)
    all_used_declared_score = allUsedIsDeclared(ast)
    all_expression_finished_score = allExpressionFinished(ast)
    indentation_score = indentation(ast)  
    number_line_score = numberLine(ast)
    
    total_score = (all_declared_used_score + all_used_declared_score +
                   all_expression_finished_score + indentation_score + number_line_score)
    normalized_score = (total_score / 50) * 10 
    
    return round(normalized_score, 2)