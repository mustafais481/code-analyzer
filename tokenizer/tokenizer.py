from . import helper
from . import constants as constTokens
import re

class Tokenizer: 
    @staticmethod
    def tokenize(code):
            # Remplacer les caractères spéciaux
            code = helper.replaceSpecialsChars(code)
                   
            _tokens = re.split(r'[\t\f\v ]+', code)

            tokens = []

            for t in _tokens:
                
                if not t or not t.isdigit():
                    # On vérifie si c'est un caractère spécial
                    type_chars = helper.checkChars(t)
                    if type_chars:
                        tokens.append({'type': type_chars})
                    # Sinon, c'est un mot
                    else:
                        tokens.append({'type': constTokens.typeWord, 'value': t})
                # Sinon, c'est un nombre
                else:
                    tokens.append({'type': constTokens.typeNumber, 'value': t})
            if len(tokens) < 1:
                raise Exception(constTokens.errorNoTokenFound)
            return tokens