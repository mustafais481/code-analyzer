specialChars = {
    'newLine': {'regRule': r'\n', 'value': '\n'},
    'endInstruct': {'regRule': r';', 'value': ';'},
    'equal': {'regRule': r'=', 'value': '='},
    'point': {'regRule': r'\.', 'value': '.'},
    'virgule': {'regRule': r',', 'value': ','},
    'quotationMark': {'regRule': r'\"', 'value': '"'},
    'openParenthese': {'regRule': r'\(', 'value': '('},
    'closeParenthese': {'regRule': r'\)', 'value': ')'},
    'tabulation': {'regRule': r'^\t', 'value':'\t'},
    "indentation" : {'regRule': r' {3,}', 'value':'  '}
}

symboleSpace = "space"
symboleEqual = "equal"
symbolePoint = "point"
symboleVirgule = "virgule"
symboleQuotationMark = "quotationMark"
symboleOpenParenthese = "openParenthese"
symboleCloseParenthese = "closeParenthese"

typeNumber = "number"
typeWord = "word"

errorNoTokenFound = 'No Tokens Found.'
