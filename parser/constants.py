typeVariable = 'variable'
typeString = 'string'

expressionDeclaration = "variableDeclaration"
expressionAffectation = "variableAffectation"
expressionMethodCall = "objectMethodCall"
indentation="indentation"
declarationVariable = ["var", "let", "const"]
variableNotDeclared="variableNotDeclared"

errorMissingOpenParenthesis = "Error: missing a open parenthesis"
errorMissingCloseParenthesis = "Error: missing a close parenthesis"
errorMissingQuotationMark = "Error: missing quotation mark"
errorMissingWord = "Error: missing a word for valid expression"
