from tokenizer import constants as constTokens
from . import constants as constParser
from . import expressionsFactory as factory

def parse(tokens):
    AST = []
    i = 0
    while i < len(tokens):
        expression = None

        if tokens[i]['type'] == constTokens.typeWord and tokens[i]['value'] in constParser.declarationVariable:
            expression = factory.create(constParser.expressionDeclaration, tokens, i)
            i += 1

        elif tokens[i]['type'] == constTokens.symboleEqual:
            expression = factory.create(constParser.expressionAffectation, tokens, i)

            if expression['variableValue']['type'] == constTokens.typeNumber:
                i += 1

            else:
                i = expression['variableValue']['end']
                
        elif i < len(tokens) - 1 and tokens[i]['type'] == constTokens.typeWord and tokens[i + 1] == constTokens.symbolePoint:
            expression = factory.create(constParser.expressionMethodCall, tokens, i)
            i = expression['end']

        # elif i < len(tokens) -1 and tokens[i]['type'] == constTokens.typeWord and tokens[i - 1] not in constParser.declarationVariable:
        #     expression = factory.create(constParser.variableNotDeclared, tokens, i)

        if expression:
            AST.append(expression)
        else:
            AST.append(tokens[i])
        i += 1
    return AST
